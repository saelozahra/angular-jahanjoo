/*
 * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

/**
 * امکانات اولیه برای مکان‌یابی را در اختیار کاربران قرار می‌دهد.
 */
angular.module('pluf.jahanjoo', [ 'pluf' ]);

/*
 * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('pluf.jahanjoo')

/**
 * ساختار داده‌ای یک مکان را ایجاد می‌کند. علاوه بر این ابزارهای اولیه مورد نیاز
 * برای دستکاری مکان را نیز در اختیار می‌گذارد
 */
.factory('PLocation', function($http, PObject) {
	/**
	 * یک نمونه جدید از این کلاس ایجاد می‌کند.
	 */
	var pLocation = function() {
		PObject.apply(this, arguments);
	};
	pLocation.prototype = new PObject();

	/**
	 * این مکان را از سیستم حذف می‌کند.
	 */
	pLocation.prototype.remove = function() {
		var scope = this;
		return $http({
			method : 'DELETE',
			url : '/api/jayab/location/' + this.id,
		})//
		.then(function(res) {
			scope.setData(res);
			scope.id = 0;
			return scope;
		});
	};

	/**
	 * خصوصیت‌های مکان را به روز می‌کند.
	 * 
	 */
	pLocation.prototype.update = function() {
		// TODO: maso, 1395: پیاده سازی
	};

	/**
	 * دسترسی به یک تگ خاص
	 */
	pLocation.prototype.tag = function() {
		// TODO: maso, 1395: پیاده سازی
	};

	/**
	 * فهرست تمام تگ‌های یک مکان
	 */
	pLocation.prototype.tags = function() {
		// TODO: maso, 1395: پیاده سازی
	};

	/**
	 * اضافه کردن یک تگ چدید
	 */
	pLocation.prototype.newTag = function() {
		// TODO: maso, 1395: پیاده سازی
	};

	// returns module
	return pLocation;
});

/*
 * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('pluf.jahanjoo')
/**
 * ابزارهای مورد نیاز برای یک برچسب را ایجاد می‌کند.
 */
.factory('PTag', function(PObject) {
	var pTag = function() {
		pTag.apply(this, arguments);
	};
	pTag.prototype = new PObject();
	pTag.prototype.update = function() {
		// TODO: maso, 1395: پیاده سازی
	};
	pTag.prototype.remove = function() {
		// TODO: maso, 1395: پیاده سازی
	};
	return pTag;
});

/*
 * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('pluf.jahanjoo')

/**
 * ابزارهای موردنیاز برای تعیین یک رای را ایجاد می‌کند.
 */
.factory('PVote', function(PObject) {
	var pVote = function() {
		PObject.apply(this, arguments);
	};
	pVote.prototype = new PObject();
	pVote.prototype.update = function() {
		// TODO: maso, 1395: پیاده سازی
	};
	pVote.prototype.remove = function() {
		// TODO: maso, 1395: پیاده سازی
	};
	return pVote;
});
/*
 * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('pluf.jahanjoo')

/**
 * @memberof pluf.jahanjoo
 * @component service
 * 
 * @description سرویس مدیرت مکان را به صورت مجازی فراهم می‌کند.
 */
.service(
		'$jahanjoo',
		function($http, $httpParamSerializerJQLike, $q, PLocation,
				PaginatorPage, PObjectCache) {

			var postct = 'application/x-www-form-urlencoded';
			var _localCache = new PObjectCache(function(data) {
				return new PLocation(data);
			});
			this._localCache = _localCache;

			/**
			 * گرفتن اطلاعات یک مکان
			 */
			this.location = function(id) {
				if (_localCache.contains(id)) {
					var deferred = $q.defer();
					deferred.resolve(_localCache.get(id));
					return deferred.promise;
				}
				return $http({
					method : 'GET',
					url : '/api/jayab/location/' + id,
				}).then(function(res) {
					return _localCache.restor(id, res.data);
				});
			};

			/**
			 * فهرستی از تمام مکان‌های اضافه شده در سیستم.
			 */
			this.locations = function(pagParam) {
				var param = {};
				if (pagParam) {
					param = pagParam.getParameter();
				}
				return $http({
					method : 'GET',
					url : '/api/jayab/location/find',
					params : param
				}).then(function(res) {
					var page = new PaginatorPage(res.data);
					page.items = [];
					for (var i = 0; i < res.data.counts; i++) {
						var item = res.data.items[i];
						page.items.push(_localCache.restor(item.id, item));
					}
					return page;
				});
			};

			/**
			 * یک مکان جدید را در سیستم تعریف می‌کند این مکان باید به صورت زیر
			 * ایجاد بشه:
			 * 
			 * <pre><code>
			 * {
			 * 	name : title,
			 * 	description : description,
			 * 	latitude : lat,
			 * 	longitude : long
			 * }
			 * </code></pre>
			 */
			this.newLocation = function(locationData) {
				return $http({
					method : 'POST',
					url : '/api/jayab/location/new',
					data : $httpParamSerializerJQLike(locationData),
					headers : {
						'Content-Type' : postct
					}
				}).then(function(res) {
					return _localCache.restor(res.data.id, res.data);
				});
			};

			this.tag = function() {
			};
			this.tags = function() {
			};
			this.newTag = function() {
			};

		});
